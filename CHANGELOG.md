## 0.1.2

* Android : improve connection to secured network

## 0.1.1

* iOs : Add support to connect to secured network

## 0.1.0

* Add support to get SSID
* Add basic support to connect to Wifi
* Android : add methods to route connection to Wifi with no internet 
