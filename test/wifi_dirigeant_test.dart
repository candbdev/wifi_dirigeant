import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:wifi_dirigeant/wifi_dirigeant.dart';

void main() {
  const MethodChannel channel = MethodChannel('wifi_dirigeant');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await WifiDirigeant.platformVersion, '42');
  });
}
