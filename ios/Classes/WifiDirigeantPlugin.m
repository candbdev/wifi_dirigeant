#import "WifiDirigeantPlugin.h"
#import <wifi_dirigeant/wifi_dirigeant-Swift.h>

@implementation WifiDirigeantPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftWifiDirigeantPlugin registerWithRegistrar:registrar];
}
@end
